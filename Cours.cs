namespace CoursCsharp
{
    class MainProgram
    {
        static void Main(string[] args)
        {
           Console.WriteLine("Hello World");
        }
    }

    class Matrix
    {
        //Champs
        private int[,] _matrix;
        private int _size;

        //Proprietes
        public int[,] matrix
        {
            get
            {
                return _matrix;
            }
            set
            {
                _matrix = value;
            }
        }
        public int size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }

        //Constructeurs
        public Matrix()
        {
            this._matrix = new int[3,3];
            this._size = 3;
            for (int i = 0; i < this._size; i++)
            {
                for (int j = 0; j < this._size; j++)
                {
                    this._matrix[i,j] = 9;
                }
            }
        }

        public Matrix(int size_, int DefautVal)
        {
            this._size = size_;
            this._matrix = new int[this._size,this._size];
            for (int i = 0; i < this._size; i++)
            {
                for (int j = 0; j < this._size; j++)
                {
                    this._matrix[i,j] = DefautVal;
                }
            }
        }
    }
}

/*
Le namespace est l'espace de nom, c'est ce qui permet de regrouper differents fichiers au sein d'un meme ensemble.
Deux classes dans le meme namespace pourront s'appeller mutuellement, ce qui n'est pas vrai dans le cas inverse.
Un espace de nom peut etre importe au projet en utilisant "Using NomNameSpace;"
*****************
Une classe est un ensemble de champs/propriete et de methodes creant un objet. Elles peuvent etre instancier dans notre programme a plusieurs reprise.
Une instance est une copie, en faisant une instance d'une classe, on creer un objet.
*****************
Une methode est une fonction ou une procedure.
Une procedure est une fonction qui ne renvoie rien.
Une fonction peut prendre en entree des arguments (Ou parametres), effectue (ou pas) des instructions et elle renvoie quelque chose.

Une classe peut avoir des methodes de classe et des methodes d'instance.
Une methode de classe peut etre utilisee sans avoir besoin d'instancier votre classe (objet)
Une methode d'instance ne peut etre utilisee que lorsque vous avez instancie votre classe.
*****************
Un champs est un membre d'une classe, il est comme une variable d'un objet.
Un champs est principalement en private car un champs public briserait le concepte d'encapsulation de la prog objet.

Du coup il y a des Proprietes, en fait ce sont des methodes speciales qu'on appelle des accesseurs.
Ces accesseurs sont un moyen de recuperer et d'attribuer la valeur d'un champs qui est cense etre en private.
*****************
Une classe contient aussi un ou plusieurs Constructeurs.
Un constructeur est ce qui permet litteralement de construire votre objet, lorsque vous instancierez votre classe et donc votre objet, le programme commencera par construire votre objet.
Un constructeur peut prendre des Parametres tout comme une methode.
On creer le constructeur en utilisant la forme "public NomDeClasse(Param1, Param2...){code;}"
Comme je l'ai dit, il est possible d'avoir plusieurs constructeurs, dans la classe Matrice de mon exemple il y a deux constructeurs.
Nous instancions notre classe avec "Matrix MaMatrice = new Matrix(5,0);" pour avoir une matrice carree de 5 par 5 avec la valeur 0 dans chaque case.
Dans l'exemplie, nous avons un constructeur sans parametre, on l'instancie donc "Matrix MaMatrice = new Matrix()" et d'apres le code de mon exemple, nous aurons une matrice de 3 par 3 avec la valeur 9 partout.
Ce serait un peu comme un constructeur par defaut.

*/
